use serde::Deserialize;
use anyhow::Result;

#[derive(Deserialize, Debug)]
pub struct Station {
    pub leitpunktkürzel: String,
    pub tarifpunkt: String,
    pub ds100: String,
}

#[derive(Debug)]
pub struct Stations {
    pub stations: Vec<Station>,
}

impl Stations {
    pub fn new() -> Self {
        Stations { stations: vec![] }
    }
    pub fn from_file(file: &str) -> Result<Stations> {
        let file = std::fs::File::open(file)?;
        let mut reader = csv::ReaderBuilder::new().delimiter(b';').from_reader(file);
        let mut stations = Stations::new();
        for result in reader.deserialize() {
            let record: Station = result?;
            stations.stations.push(record);
        }
        log::info!("Loaded {} stations", stations.stations.len());
        Ok(stations)
    }
    pub fn find_leitpunkt(&self, lpk: &str) -> Option<&Station> {
        for station in &self.stations {
            if station.leitpunktkürzel == lpk {
                return Some(station)
            }
        }
        None
    }
}
