use crate::stations::Stations;
use crate::via::{parse_via, ViaComponent};
use actix_files::NamedFile;
use actix_web::{get, web, HttpRequest, HttpResponse, Responder, Result};

#[get("/")]
async fn index(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("www/index.html")?)
}

fn format_via(obj: ViaComponent, stations: &Stations) -> String {
    match obj {
        ViaComponent::Line(line) => {
            line.into_iter()
                .map(|s| format_via(s, stations))
                .collect::<Vec<String>>()
                .join(" * ")
        }
        ViaComponent::Leitpunkt(lpk) => {
            match stations.find_leitpunkt(lpk) {
                Some(station) => station.tarifpunkt.clone(),
                None => String::from("Unknown"),
            }
        }
        ViaComponent::Raumbegrenzung(top, bottom) => {
            format!("( {} / {} )", format_via(*top, stations), format_via(*bottom, stations))
        }
    }
}

#[get("/via/{code}")]
async fn via(code: web::Path<String>, data: web::Data<Stations>) -> impl Responder {
    log::debug!("Received {code}");
    match parse_via(&code) {
        Err(err) => {
            HttpResponse::InternalServerError() //??
                .body(format!("Encountered an error: {:?}", err))
        }
        Ok(("", begrenzung)) => {
            let response = format_via(begrenzung, &data);
            HttpResponse::Ok().body(response)
        }
        Ok((rem, _)) => {
            HttpResponse::BadRequest().body(format!("Trailing garbage in via code:\n {}", rem))
        }
    }
}

#[get("/station/{lpk}")]
async fn station_leitpunkt(lpk: web::Path<String>, data: web::Data<Stations>) -> impl Responder {
    match data.find_leitpunkt(&lpk) {
        Some(station) => HttpResponse::Ok().body(format!("{}", station.tarifpunkt)),
        None => HttpResponse::NotFound().body(format!("No station {}", lpk)),
    }
}
