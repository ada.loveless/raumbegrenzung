use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::alpha1,
    multi::separated_list1,
    sequence::{delimited, separated_pair},
    IResult,
};

#[derive(Debug, PartialEq)]
pub enum ViaComponent<'a> {
    Leitpunkt(&'a str),
    Line(Vec<ViaComponent<'a>>),
    Raumbegrenzung(Box<ViaComponent<'a>>, Box<ViaComponent<'a>>),
}

pub fn parse_leitpunkt(stream: &str) -> IResult<&str, ViaComponent> {
    let (stream, station) = alpha1(stream)?;
    Ok((stream, ViaComponent::Leitpunkt(station)))
}

pub fn parse_line(stream: &str) -> IResult<&str, ViaComponent> {
    let (stream, stations) = separated_list1(tag("*"), parse_leitpunkt)(stream)?;
    Ok((stream, ViaComponent::Line(stations)))
}

#[test]
fn test_parse_line() {
    let cases = [
        (
            "A",
            (ViaComponent::Line(vec![ViaComponent::Leitpunkt("A")])),
        ),
        (
            "A*B",
            (ViaComponent::Line(vec![
                ViaComponent::Leitpunkt("A"),
                ViaComponent::Leitpunkt("B"),
            ])),
        ),
        (
            "A*B*C",
            (ViaComponent::Line(vec![
                ViaComponent::Leitpunkt("A"),
                ViaComponent::Leitpunkt("B"),
                ViaComponent::Leitpunkt("C"),
            ])),
        ),
    ];
    for (code, expected) in cases {
        let (rem, result) = parse_line(code).unwrap();
        println!("Input: '{code}'\nExpected: '{expected:?}'\n  Result: '{result:?}'");
        assert!(rem == "");
        assert!(result == expected);
    }
}

fn parse_raumbegrenzung(stream: &str) -> IResult<&str, ViaComponent> {
    let (stream, (top, bottom)) = delimited(
        tag("("),
        separated_pair(parse_line, tag("/"), parse_line),
        tag(")"),
    )(stream)?;
    Ok((
        stream,
        ViaComponent::Raumbegrenzung(Box::new(top), Box::new(bottom)),
    ))
}

#[test]
fn test_parse_raumbegrenzung() {
    let cases = [
        (
            "(A/B)",
            ViaComponent::Raumbegrenzung(
                Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("A")])),
                Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("B")])),
            ),
        ),
        (
            "(A/B*C)",
            ViaComponent::Raumbegrenzung(
                Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("A")])),
                Box::new(ViaComponent::Line(vec![
                    ViaComponent::Leitpunkt("B"),
                    ViaComponent::Leitpunkt("C"),
                ])),
            ),
        ),
        (
            "(X*Y*Z/B*C)",
            ViaComponent::Raumbegrenzung(
                Box::new(ViaComponent::Line(vec![
                    ViaComponent::Leitpunkt("X"),
                    ViaComponent::Leitpunkt("Y"),
                    ViaComponent::Leitpunkt("Z"),
                ])),
                Box::new(ViaComponent::Line(vec![
                    ViaComponent::Leitpunkt("B"),
                    ViaComponent::Leitpunkt("C"),
                ])),
            ),
        ),
        (
            "(X*Y*Z/A)",
            ViaComponent::Raumbegrenzung(
                Box::new(ViaComponent::Line(vec![
                    ViaComponent::Leitpunkt("X"),
                    ViaComponent::Leitpunkt("Y"),
                    ViaComponent::Leitpunkt("Z"),
                ])),
                Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("A")])),
            ),
        ),
    ];
    for (code, expected) in cases {
        let (rem, result) = parse_raumbegrenzung(code).unwrap();
        println!("Input: '{code}'\nExpected: '{expected:?}'\n  Result: '{result:?}'");
        assert!(rem == "");
        assert!(result == expected);
    }
}

pub fn parse_via(stream: &str) -> IResult<&str, ViaComponent> {
    let (stream, stations) =
        separated_list1(tag("*"), alt((parse_leitpunkt, parse_raumbegrenzung)))(stream)?;
    Ok((stream, ViaComponent::Line(stations)))
}

#[test]
fn test_parse_via() {
    let cases = [
        (
            "Z*(A/B)*Y",
            ViaComponent::Line(vec![
                ViaComponent::Leitpunkt("Z"),
                ViaComponent::Raumbegrenzung(
                    Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("A")])),
                    Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("B")])),
                ),
                ViaComponent::Leitpunkt("Y"),
            ]),
        ),
        (
            "(C/D*E)*Z*(A/B)*Y",
            ViaComponent::Line(vec![
                ViaComponent::Raumbegrenzung(
                    Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("C")])),
                    Box::new(ViaComponent::Line(vec![
                        ViaComponent::Leitpunkt("D"),
                        ViaComponent::Leitpunkt("E"),
                    ])),
                ),
                ViaComponent::Leitpunkt("Z"),
                ViaComponent::Raumbegrenzung(
                    Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("A")])),
                    Box::new(ViaComponent::Line(vec![ViaComponent::Leitpunkt("B")])),
                ),
                ViaComponent::Leitpunkt("Y"),
            ]),
        ),
    ];
    for (code, expected) in cases {
        let (rem, result) = parse_via(code).unwrap();
        println!("Input: '{code}'\nExpected: '{expected:?}'\n  Result: '{result:?}'\n Remainder: {rem:?}");
        assert!(rem == "");
        assert!(result == expected);
    }
}
