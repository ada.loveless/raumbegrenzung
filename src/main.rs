mod stations;
mod via;
mod server;

use clap::Parser;
use actix_web::{web, App, HttpServer};

#[derive(Parser)]
#[command(author, version, about)]
struct Args {
    /// File with station data
    #[arg(short, long)]
    data: String,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    let args = Args::parse();
    let stations = stations::Stations::from_file(&args.data).expect("Unable to load data");
    let data = web::Data::new(stations);
    HttpServer::new(move || {
        App::new()
            //.route("/{filename:.*}", web::get().to(server::index))
            //.service(actix_files::Files::new("/", "www").show_files_listing())
            .service(server::index)
            .service(server::station_leitpunkt)
            .service(server::via)
            .app_data(data.clone())
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
